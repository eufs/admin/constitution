# Constitution

This project contains the most up-to-date LaTeX source code for the EUFS constitution. To edit, simply upload `main.tex` and `figs` into overleaf (or any other LaTeX editor). If you make any changes to the Constitution, make sure you update the pdf accordingly - this will aid readability. 
